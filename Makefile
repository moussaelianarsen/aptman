SRCDIR ?= .

aptman: aptman.go
	go build aptman.go

install: aptman
	install -Dm755 $(SRCDIR)/aptman $(DESTDIR)/usr/bin/aptman